<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:param name="NAMESPACE"/>
<xsl:param name="PROJECTNAME"/>

<xsl:template match="/index">
<html>
<head>
<title>oVirt 4.0</title>
</head>
<body>
This is a YUM repository of latest oVirt 4.0 packages built for CentOS 7.
<p/>
YUM repository config:
<pre>
[ovirt40]
name=Latest oVirt 4.0 packages
baseurl=https://<xsl:value-of select="$NAMESPACE"/>.gitlab.io/<xsl:value-of select="$PROJECTNAME"/>/RPMS
enabled=1
skip_if_unavailable=1
gpgcheck=0

[ovirt40-source]
name=Latest oVirt 4.0 packages - Source
baseurl=https://<xsl:value-of select="$NAMESPACE"/>.gitlab.io/<xsl:value-of select="$PROJECTNAME"/>/SRPMS
enabled=0
skip_if_unavailable=1
gpgcheck=0
</pre>
<p/>
List of files:
<pre>
<xsl:apply-templates select="file"/>
</pre>
<p/>
Build <a href="https://gitlab.com/{$NAMESPACE}/{$PROJECTNAME}/pipelines">logs</a>
</body>
</html>
</xsl:template>

<xsl:template match="/index/file">
<a href="{@name}">
    <xsl:value-of select="@name"/>
</a><br/>
</xsl:template>

</xsl:stylesheet>
